<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of File
 *
 * @author pablo
 */
class File {
    
   public static function upload($target_dir,$file,$title = ''){
       
       if($_FILES[$file]["error"] > 0){
           exit("Error en la subida del archivo");
       }
       $ext = end(explode(".",basename($_FILES[$file]["name"])));

       $target_file = ($title != '') ? $title.".".$ext :basename($_FILES[$file]["name"]);
       
       if( move_uploaded_file($_FILES[$file]["tmp_name"], $target_dir.$target_file)){
           return true;
       }else{
           return false;
       }
       
   }
    
}
