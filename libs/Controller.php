<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Controller
 *
 * @author pablo
 */
class Controller {
    
    function __construct() {
        
        $this->view = new View();
        $this->loadModel();
    }
    
    function loadModel(){
        
        $model = substr(get_class($this),0,-11).'_model'; //Controller_model
        $path = './models/'.$model.'.php';
        
        if(file_exists($path)){
            require $path;
            $this->model = new $model();
        }
        
    }
    
    
    
}
spl_autoload_register(function($class){
    if(file_exists("./controllers/".$class.".php")){
        require "./controllers/".$class.".php";
    }
});
