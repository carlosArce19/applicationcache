<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of View
 *
 * @author pablo
 */
class View {
    
    function render($controller,$view){
        
        $controller = get_class($controller);
        $controller = substr($controller, 0,-11);
        $path = './views/'.$controller.'/'.$view;
        if(file_exists($path.".php")){
           require $path.".php"; 
        }elseif(file_exists($path.".html")){
            require $path.".html"; 
        }else{
            echo "Error: Invalid view ".$view." to render";
        }
        
    }
}
