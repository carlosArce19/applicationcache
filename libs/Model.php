<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 /* Description of Model
 *
 * @author pabhoz
 */
class Model {
    
    protected static $table;
            
    function __construct() {
            $this->db = new MySQLiManager(_DB_HOST,_DB_USER,_DB_PASS,_DB_NAME); //MySQLiManager
    }
    
    function save($data){
        return $this->db->insert(self::$table,$data);
    }
    
    function getBy($field,$value){
        
        return array_shift($this->db->select('*',self::$table,$field.' = '.$value));
        
    }
    
    function find($field,$value){
        return $this->db->select('*',self::$table,$field.' LIKE \'%'.$value.'%\';');
        
    }

    public static function setTable($table) {
        self::$table = $table;
    }
    
    public function get(){
        return $this->db->select('*',self::$table);
        
    }
    
    public function delete($id){
        return $this->db->delete(self::$table,"id = ".$id);
    }
    
    public function update($data,$where){
        return $this->db->update(self::$table,$data,$where);
    }
}
