<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Audio
 *
 * @author pablo
 */
class Cancion extends File{
    
    private $id,$titulo,$ano,$genero,$reproducciones,$archivo;
    
    public function __construct($id,$titulo,$ano,$genero,
            $reproducciones,$archivo) {
        $this->id = $id;
        $this->titulo = $titulo;
        $this->ano = $ano;
        $this->genero = $genero;
        $this->archivo = $archivo;
        $this->reproducciones = $reproducciones;
    }
    
    function getId() {
        return $this->id;
    }

    function getTitulo() {
        return $this->titulo;
    }

    function getAno() {
        return $this->ano;
    }

    function getGenero() {
        return $this->genero;
    }

    function getReproducciones() {
        return $this->reproducciones;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    function setAno($ano) {
        $this->ano = $ano;
    }

    function setGenero($genero) {
        $this->genero = $genero;
    }

    function setReproducciones($reproducciones) {
        $this->reproducciones = $reproducciones;
    }
    function getArchivo() {
        return $this->archivo;
    }

    function setArchivo($archivo) {
        $this->archivo = $archivo;
    }

    
        public static function upload($target_dir,$file,$title=''){
        
        parent::upload($target_dir, $file , $title);
        $ext = end(explode(".",basename($_FILES[$file]["name"])));
        $target_file = ($title != '') ? $title.".".$ext :basename($_FILES[$file]["name"]);
       
        return $target_dir.$target_file;
        
    }
    function toArray(){
        $arr = get_object_vars($this);
        print_r($arr);
        foreach ($arr as $key => $value) {
            $arr[$key] = $this->{"get".ucfirst($key)}();
        }
        return $arr;
    }
    
}
