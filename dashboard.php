<!DOCTYPE html>

<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8">
	<title>MyPlayer App</title>
	<link rel="stylesheet" href="./css/app.css">
	<link href="./assets/ionicons-2.0.1/css/ionicons.css" rel="stylesheet" type="text/css">
	<script src="./js/jquery/jquery-2.1.4.js"></script>
	<script src="./js/app.js"></script>
</head>

<body onload="initApp()">
	<div class="panelIzq"> 
	<!-- Foto arriba 
		!-->
       <div class= "foto-perfil">
     
       </div>
       <!-- Bsrra de herramientas con opciones de tipo block 
		!-->
		<div class="barraH">
       	<div class="subir"><i class="ion-android-archive"></i> Subir canción</div>
       	<div class ="eliminar"><i class="ion-android-delete"></i> Eliminar</div>
       	<div class="crear-lista"><i class="ion-android-list"></i> Crear lista</div>
       	<div class="editar-lista"><i class="ion-compose"></i> Editar lista</div>
       	<div class="eliminar-lista"><i class="ion-android-delete"></i> Eliminar Lista</div>
       	<div class="editar-perfil"><i class="ion-edit"></i> Editar Perfil</div>
       </div>
     
	</div>
      <!-- Workspace quiere decir  que se clickea en la barra de herramientas pero se debe desplegar las acciones en el workspace, esto debe quedar en el centro. 
		!-->
	<div class="workspace"></div>
      <!-- Replica pequeña del timeline al lado izquierdo en un panel 
		!-->
	<div class="mini-timeline">
</div>
</body>

</html>